"use client"
import { Inter } from 'next/font/google'
import { useEffect, useState } from 'react'

const inter = Inter({ subsets: ['latin'] })



export default function Home() {
  let [data, setData] = useState<string[]>()
  const getData = async () => {
    const res = await fetch(`http://localhost:3000/api/ingredients`)
    const data = await res.json()
    setData(data)
  }

  useEffect(() => {
    getData()
  }, [])

  useEffect(() => {
    console.log('data', data)
  }, [data])

  return (
    <div className='m-10'>
      <h1>Lista składników:</h1>
      <ul>
        {data?.map((elem, index) => {
          return <li key={index}>{elem}</li>
        })}
      </ul>
    </div>
  )
}

// export async function getServerSideProps() {
//   // Fetch data from external API
//   const res = await fetch(`http://localhost:3000/api/ingredients`)
//   const data = await res.json()

//   // Pass data to the page via props
//   return { props: { data } }
// }
